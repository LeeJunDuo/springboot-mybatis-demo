CREATE TABLE IF NOT EXISTS movie (
    id SERIAL PRIMARY KEY,
    name varchar(32) NOT NULL,
    dt date NOT NULL
);
